//
//  Contact+Extension.swift
//  Contacts
//
//  Created by Dmitry Baginsky on 7/25/19.
//  Copyright © 2019 Dmitry Baginsky. All rights reserved.
//

import Foundation

extension Contact {
  
  convenience init() {
    self.init(context: CoreDataService.shared.persistentContainer.viewContext)
    id = String.uniqueId()
  }
  
  var fullName: String {
    return "\(firstName ?? "") \(lastName ?? "")"
  }
  
  func load(with firstName: String, lastName: String, numbers: [String]) {
    self.firstName = firstName
    self.lastName = lastName
    self.numbers = NSSet(array: numbers.map {
      let number = Number(context: CoreDataService.shared.persistentContainer.viewContext)
      number.value = $0
      return number
    })
  }
}

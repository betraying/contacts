//
//  AppDelegate.swift
//  Contacts
//
//  Created by Dmitry Baginsky on 7/25/19.
//  Copyright © 2019 Dmitry Baginsky. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    return true
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    CoreDataService.shared.saveContext()
  }
}


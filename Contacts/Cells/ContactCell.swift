//
//  ContactCell.swift
//  Contacts
//
//  Created by Dmitry Baginsky on 7/25/19.
//  Copyright © 2019 Dmitry Baginsky. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {
  
  func load(with contact: Contact) {
    textLabel?.text = contact.fullName
    
    let numbers = contact.numbers?.allObjects as? [Number] ?? []
    detailTextLabel?.text = numbers
      .compactMap { $0.value }
      .sorted()
      .joined(separator: ", ")
  }
}

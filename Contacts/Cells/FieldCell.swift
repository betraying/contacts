//
//  FieldCell.swift
//  Contacts
//
//  Created by Dmitry Baginsky on 7/25/19.
//  Copyright © 2019 Dmitry Baginsky. All rights reserved.
//

import UIKit

class FieldCell: UITableViewCell {
  
  enum Mode {
    case firstName, lastName, number
  }
  
  var changed: ((String) -> Void)?
  
  @IBOutlet weak var textField: UITextField!
  
  var mode: Mode = .firstName {
    didSet {
      switch mode {
      case .firstName:
        textField.placeholder = "First Name"
        textField.keyboardType = .default
      case .lastName:
        textField.placeholder = "Last Name"
        textField.keyboardType = .default
      case .number:
        textField.placeholder = "Mobile phone"
        textField.keyboardType = .phonePad
      }
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    textField.addTarget(self, action: #selector(textChanged), for: .editingChanged)
  }
  
  func load(with text: String, mode: Mode, changed: ((String) -> Void)?) {
    self.textField.text = text
    self.mode = mode
    self.changed = changed
  }
  
  @objc func textChanged() {
    changed?(textField.text ?? "")
  }
}

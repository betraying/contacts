//
//  CoreDataService.swift
//  Contacts
//
//  Created by Dmitry Baginsky on 7/25/19.
//  Copyright © 2019 Dmitry Baginsky. All rights reserved.
//

import Foundation
import CoreData

final class CoreDataService {
  
  static let shared = CoreDataService()
  private init() {}
  
  lazy var persistentContainer: NSPersistentContainer = {
    let container = NSPersistentContainer(name: "Contacts")
    container.loadPersistentStores(completionHandler: { (storeDescription, error) in
      if let error = error as NSError? {
        fatalError("Unresolved error \(error), \(error.userInfo)")
      }
    })
    return container
  }()
  
  func saveContext() {
    let context = persistentContainer.viewContext
    if context.hasChanges {
      do {
        try context.save()
      } catch {
        let nserror = error as NSError
        fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
      }
    }
  }
  
  func deleteEntity(_ entity: NSManagedObject) {
    persistentContainer.viewContext.delete(entity)
  }
  
  func fetchContact(with id: String?) -> Contact? {
    return fetchContacts().first { $0.id == id }
  }
  
  func fetchContacts() -> [Contact] {
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
    request.returnsObjectsAsFaults = false
    request.sortDescriptors = [NSSortDescriptor(key: "firstName", ascending: true)]
    do {
      return try persistentContainer.viewContext.fetch(request) as? [Contact] ?? []
    } catch {
      return []
    }
  }
}

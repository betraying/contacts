//
//  DetailViewController.swift
//  Contacts
//
//  Created by Dmitry Baginsky on 7/25/19.
//  Copyright © 2019 Dmitry Baginsky. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  
  var firstName: String = ""
  var lastName: String = ""
  var numbers: [String] = []
  
  private var contact: Contact? {
    didSet {
      guard let contact = contact else { return }
      firstName = contact.firstName ?? ""
      lastName = contact.lastName ?? ""
      let numbers = contact.numbers?.allObjects as? [Number] ?? []
      self.numbers = numbers.compactMap { $0.value }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(actionTap))
    view.addGestureRecognizer(tapGesture)
    
    navigationItem.rightBarButtonItems = [
      .init(barButtonSystemItem: .save, target: self, action: #selector(actionSave)),
      .init(barButtonSystemItem: .add, target: self, action: #selector(actionAddNumber))
    ]
    
    if contact == nil {
      numbers.append(String())
    }
  }
  
  @objc func actionTap() {
    view.endEditing(true)
  }
  
  func load(contact: Contact) {
    self.contact = contact
  }
  
  @objc func actionAddNumber() {
    let indexPath = IndexPath(row: numbers.count + 2, section: 0)
    numbers.append("")
    tableView.insertRows(at: [indexPath], with: .automatic)
  }
  
  @objc func actionSave() {
    guard !firstName.isEmpty, !lastName.isEmpty, !numbers.contains("") else { return }
    
    if let contact =  CoreDataService.shared.fetchContact(with: contact?.id) {
      contact.load(with: firstName, lastName: lastName, numbers: numbers)
    } else {
      let contact = Contact()
      contact.load(with: firstName, lastName: lastName, numbers: numbers)
    }
    
    CoreDataService.shared.saveContext()
    
    navigationController?.popViewController(animated: true)
  }
}

extension DetailViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 2 + numbers.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "field") as? FieldCell else {
      return UITableViewCell()
    }
    switch indexPath.row {
    case 0:
      cell.load(with: firstName, mode: .firstName) { [weak self] in
        self?.firstName = $0
      }
    case 1:
      cell.load(with: lastName, mode: .lastName) { [weak self] in
        self?.lastName = $0
      }
    default:
      let number = numbers[indexPath.row - 2]
      cell.load(with: number, mode: .number) { [weak self] in
        self?.numbers[indexPath.row - 2] = $0
      }
    }
    return cell
  }
}

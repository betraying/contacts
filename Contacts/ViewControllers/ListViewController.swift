//
//  ListViewController.swift
//  Contacts
//
//  Created by Dmitry Baginsky on 7/25/19.
//  Copyright © 2019 Dmitry Baginsky. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  var contacts: [Contact] = []
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    reloadData()
  }
  
  func reloadData() {
    DispatchQueue.main.async {
      self.contacts = CoreDataService.shared.fetchContacts()
      self.tableView.reloadData()
    }
  }
  
  @IBAction func actionAdd(_ sender: Any) {
    let identifier = String(describing: DetailViewController.self)
    guard let viewController = storyboard?.instantiateViewController(withIdentifier: identifier) as? DetailViewController else {
      return
    }
    
    navigationController?.pushViewController(viewController, animated: true)
  }
}

extension ListViewController: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let identifier = String(describing: DetailViewController.self)
    guard let viewController = storyboard?.instantiateViewController(withIdentifier: identifier) as? DetailViewController else {
      return
    }
    
    let contact = contacts[indexPath.row]
    viewController.load(contact: contact)
    navigationController?.pushViewController(viewController, animated: true)
  }
  
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    let contact = contacts[indexPath.row]
    
    CoreDataService.shared.deleteEntity(contact)
    CoreDataService.shared.saveContext()
    
    reloadData()
  }
}

extension ListViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return contacts.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? ContactCell else {
      return UITableViewCell()
    }
    
    let contact = contacts[indexPath.row]
    cell.load(with: contact)
    
    return cell
  }
}
